﻿// Arthur
// 06 Sep 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class Player : MonoBehaviour
{
    #region Fields

    [ Header ( "Movimente Variables" ) ]
    [ SerializeField ] private float m_speed = 10f;
    [ SerializeField ] private float m_jumpForce = 200f;
    [ SerializeField ] private float m_radius = 0.2f;

    [ SerializeField ] private Transform m_groundCheck = null;
    [ SerializeField ] private LayerMask m_layerFloor = new LayerMask ( );

    private bool m_isJumping = false;
    private bool m_isOnFloor = false;

    [ SerializeField ] private Rigidbody2D m_playerRigidbody = null;
    [ SerializeField ] private Animator m_playerAnimator = null;
    [ SerializeField ] private SpriteRenderer m_spriteRenderer = null;

    [ Header ( "Atack Check" ) ]
    [ SerializeField ] private Transform m_atackCheck = null;
    [ SerializeField ] private float m_radiusAtack = 0.5f;
    [ SerializeField ] private LayerMask m_layerMaskEnemy = new LayerMask ( );
    [ SerializeField ] private float m_timeNextAtack = 0f;

    #endregion

    #region MonoBehavior Methods

    private void Start ( )
    {
        
    }

    private void Update ( )
    {
        //m_isOnFloor = Physics2D.OverlapCircle ( m_groundCheck.position, m_radius, m_layerFloor );

        m_isOnFloor = m_playerRigidbody.IsTouchingLayers ( m_layerFloor );

        if ( Input.GetButtonDown ( "Jump" ) && m_isOnFloor == true ) m_isJumping = true;

        if ( m_timeNextAtack <= 0 )
        {
            if ( Input.GetButtonDown ( "Atack" ) && m_playerRigidbody.velocity == new Vector2 ( 0f, 0f ) )
            {
                m_playerAnimator.SetTrigger ( "atack" );
                m_timeNextAtack = 0.5f;
                //PlayerAtack ( );
            }
        }
        else 
            m_timeNextAtack -= Time.deltaTime;

        PlayerAnimation ( );
    }

    private void FixedUpdate ( )
    {
        float move = Input.GetAxis ( "Horizontal" );

        m_playerRigidbody.velocity = new Vector2 ( move * m_speed, m_playerRigidbody.velocity.y );

        if ( ( move > 0 && m_spriteRenderer.flipX == true ) || ( move < 0 && m_spriteRenderer.flipX == false ) ) Flip ( );

        if ( m_isJumping )
        {
            m_playerRigidbody.AddForce ( new Vector2 ( 0f, m_jumpForce ) );
            m_isJumping = false;
        }

        //forca do pulo
        if ( m_playerRigidbody.velocity.y > 0f && !Input.GetButton ( "Jump" ) )
        {
            m_playerRigidbody.velocity += Vector2.up * -0.8f;
        }


    }

    #endregion  

    private void Flip ( )
    {
        m_spriteRenderer.flipX = !m_spriteRenderer.flipX;
        m_atackCheck.localPosition = new Vector2 ( -m_atackCheck.localPosition.x, m_atackCheck.localPosition.y );
    }

    private void PlayerAnimation ( )
    {
        m_playerAnimator.SetFloat ( "vel_x", Mathf.Abs ( m_playerRigidbody.velocity.x ) );
        m_playerAnimator.SetFloat ( "vel_y", Mathf.Abs ( m_playerRigidbody.velocity.y ) );
    }

    private void PlayerAtack ( )
    {
        ushort _damage = ( ushort ) Random.Range ( 7, 13 );

        Collider2D [ ] cols = Physics2D.OverlapCircleAll ( m_atackCheck.position, m_radiusAtack, m_layerMaskEnemy );

        foreach ( Collider2D col in cols )
        {
            Enemy _enemy = col.GetComponent<Enemy> ( );

            if ( _enemy != null )
            {
                _enemy.EnemyHit ( _damage );
            }
        }
    }

    //private void OnDrawGizmos ( )
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere ( m_atackCheck.position, m_radiusAtack );
    //}
}
