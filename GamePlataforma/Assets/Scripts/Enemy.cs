﻿// Arthur
// 09 Sep 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using TMPro;

using UnityEngine;

#endregion

public class Enemy : MonoBehaviour
{
    #region Fiedls

    [ SerializeField ] private DamageFloat m_prefabDamage = null;

    #endregion

    public void EnemyHit ( ushort _value )
    {
        if ( _value >= 0 )
        {
            DamageFloat widget = Instantiate ( m_prefabDamage, new Vector2 ( transform.position.x, transform.position.y + 0.7f), Quaternion.identity );
            widget.Setup ( _value );
        }
    }
}

