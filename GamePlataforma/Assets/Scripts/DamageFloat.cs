﻿// Arthur
// 09 Sep 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using TMPro;

using UnityEngine;

#endregion

public class DamageFloat : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private TextMeshProUGUI m_textFloat = null;

    #endregion

    #region MonoBehavior 

    void Start ( )
    {
        Destroy ( gameObject, 0.5f );
    }

    #endregion

    public void Setup ( ushort _value )
    {
        m_textFloat.text = "-" + _value.ToString ( );
    }
}
